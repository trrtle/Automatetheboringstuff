# python 3
# this extractor checks if the text you copied to the clipboard contains a phone number or email addresses
# if it does it copies the number and email adress to the clipboard


import pyperclip, re

#-------------------------------------------------------------------------------------------------------------------------------

def extractor():

    phoneRegex = re.compile(r"\+\d{11}|0\d{9}") # find phone numbers in the dutch format like +31612345678 or 0612345678

    emailRegex = re.compile(r"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9._%+-]\.[a-zA-Z0-9]") # find emailadresses

    copiedText = str(pyperclip.paste()) #put the text from your clipboard in a variable

    allPhones = phoneRegex.findall(copiedText) #find all the phone numbers
    allEmails = emailRegex.findall(copiedText) #finda ll the email addresses
    allItems = allPhones + allEmails # add all the items together

    matches = []

    for item in allItems:
        matches.append(item) # put every item in the matches list.

    if len(matches) > 0: # if the there is at least one item in the matches list copy the list to your clipboard
        pyperclip.copy("\n".join(matches))
        print("copied to clipboard:")
        print("\n".join(matches))
    else:
        print("no phone numbers or email addresses found")

    input()
















extractor()
