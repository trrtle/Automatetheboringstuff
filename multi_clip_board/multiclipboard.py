#! python3
# mcb.pyw - Saves and loads pieces of text to the clipboard.
# Usage:
#   py.exe mcb.pyw save <keyword> - Saves clipboard to keyword.
#   py.exe mcb.pyw <keyword> - Loads keyword to clipboard.
#   py.exe mcb.pyw list - Loads all keywords to clipboard.

import sys, shelve, pyperclip


class mcb():


    def main(self):
        self.mcbShelf = shelve.open('mcb')

        if sys.argv[1].lower() == 'save' and len(sys.argv) == 3:
            self.save()

        elif sys.argv[1].lower() == 'list':
            self.listShelve()

        elif len(sys.argv) == 2:
            self.load()
        else:
            print('Instruction not recognized')


    def save(self):
        self.mcbShelf[sys.argv[2]] = pyperclip.paste()

    def listShelve(self):
        pyperclip.copy(str(list(self.mcbShelf.keys())))
        print(str(list(self.mcbShelf.keys())))

    def load(self):
        pyperclip.copy(self.mcbShelf[sys.argv[1]])




instance1 = mcb()
instance1.main()
